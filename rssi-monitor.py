#!/bin/python3

import os, sys
import argparse
import json
import requests
import time
import datetime

monitoredOh07 = ['OH2', 'OH3']
monitoredOh11 = ['OH0', 'OH1']

chambers = [
    ('GE11-M-20-L1-L', 7, 'OH2', 'RSSI0'),
    ('GE11-M-20-L1-L', 7, 'OH2', 'RSSI1'),
    ('GE11-M-20-L1-L', 7, 'OH2', 'RSSI2'),
    ('GE11-M-20-L2-L', 7, 'OH3', 'RSSI0'),
    ('GE11-M-20-L2-L', 7, 'OH3', 'RSSI1'),
    ('GE11-M-20-L2-L', 7, 'OH3', 'RSSI2'),
    ('GE11-M-31-L1-S', 11, 'OH0', 'RSSI0'),
    ('GE11-M-31-L1-S', 11, 'OH0', 'RSSI1'),
    ('GE11-M-31-L1-S', 11, 'OH0', 'RSSI2'),
    ('GE11-M-31-L2-S', 11, 'OH1', 'RSSI0'),
    ('GE11-M-31-L2-S', 11, 'OH1', 'RSSI1'),
    ('GE11-M-31-L2-S', 11, 'OH1', 'RSSI2')
]
rssiKeys = ['RSSI0', 'RSSI1', 'RSSI2']

p5proxies = dict(http='socks5h://127.0.0.1:5000', https='socks5h://127.0.0.1:5000')
jsonPath07 = 'http://srv-s2g18-33-01.cms:20100/urn:xdaq-application:lid=6146707/jsonUpdate'
jsonPath11 = 'http://srv-s2g18-33-01.cms:20100/urn:xdaq-application:lid=6146711/jsonUpdate'

sleepTime = 1 # 1 Hz monitoring rate

def main():
    ap = argparse.ArgumentParser(add_help=True)
    ap.add_argument('--time', type=int, help='Duration of the RSSI current monitoring, 1 point per second', default=300)
    ap.add_argument('--input', help='Previous RSSI trend file', default='rssi.txt')
    ap.add_argument('--output', help='File to write new RSSI average value', default='rssi.txt')
    ap.add_argument('--backup', help='Backup file path', default='rssi-backup.txt')
    ap.add_argument('--log', help='Directory to store the RSSI trend over time', default='log')
    options = ap.parse_args(sys.argv[1:])

    timeStamp = datetime.datetime.now()
    timeStampString = timeStamp.strftime('%Y-%m-%d %H:%M')

    try: os.makedirs(options.log)
    except OSError: pass
    logPath = timeStampString.replace(':', '').replace('-', '').replace(' ', '_')
    logPath = '%s/%s.log'%(options.log, logPath)
    
    time0 = time.time()
    averageRssiValues = dict.fromkeys(chambers, 0) 
    npoints = 0
    with open(logPath, 'w') as logFile:
        logFile.write('detector\trssi\ttime\tvalue\n')
        for sampleIndex in range(options.time):
            try:
                request07 = requests.get(jsonPath07, proxies=p5proxies)
                request11 = requests.get(jsonPath11, proxies=p5proxies)
            except requests.exceptions.ConnectionError:
                print('SSH tunnel to CMS not set')
                return 1
            txtMonitoring07, txtMonitoring11 = request07.text, request11.text
            jsonMonitoring = {7: json.loads(txtMonitoring07), 11: json.loads(txtMonitoring11)}
            if jsonMonitoring[7] is None and jsonMonitoring[11] is None:
                print('The xDAQ response is empty. Is the AMC monitoring running?')
                return 1

            elapsedTime = time.time()-time0
            print('Elapsed time: %d s'%(elapsedTime))
            for chamber in chambers:
                name, amc, oh, rssiKey = chamber
                jsonKey = '%s.%s'%(oh, rssiKey)
                rssiValue = jsonMonitoring[amc][jsonKey]
                print(name, amc, oh, rssiKey, rssiValue)
                logLine = '%s\t%s\t%s\t%d'%(name, rssiKey[-1:], elapsedTime, rssiValue)
                #print(logLine)
                logFile.write('%s\n'%(logLine))
                averageRssiValues[chamber] += rssiValue
            npoints += 1
            time.sleep(sleepTime)
    
    print('Average output:')

    with open(options.input, 'r') as inputFile:
        inputText = inputFile.read()
        # if input and output are identical, create backup:
        if options.input==options.output:
            with open(options.backup, 'w') as backupFile:
                backupFile.write(inputText)
    
    with open(options.output, 'w') as outputFile:
        outputFile.write(inputText)
        outputFile.write('\n')
        for chamber in chambers:
            name, amc, oh, rssiKey = chamber
            averageRssiValues[chamber] /= npoints
            outputLine = '%s\t%s\t%s\t%d'%(name, rssiKey[-1:], timeStampString, averageRssiValues[chamber])
            print(outputLine)
            outputFile.write('%s\n'%(outputLine))

if __name__=='__main__':
    status = main()
    sys.exit(status)
