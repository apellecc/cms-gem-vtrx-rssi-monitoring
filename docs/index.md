# VTRX RSSI monitoring at P5

All the following steps need to be performed on an SSH `lxplus` session.

## Prerequisites

Verify that you have read/write access to the folder ```/eos/home-a/apellecc/www/gem/ge11/commissioning/vtrx-rssi/```; otherwise, [contact me](mailto:antonello.pellecchia@cern.ch) to obtain access.

Follow the [tunnel setup guide](https://cmsgemonline.web.cern.ch/doc/setups-description/#ssh-connection) from an `lxplus` session to set up an SSH tunnel to the P5 GEM machine.

Install the necessary dependencies to plot the RSSI values:
```
pip3 install --user pandas matplotlib
```

## Update the RSSI monitor trend

Start a miniDAQ run from [RCMS](http://cmsrc-gem.cms:20000/rcms/gui/servlet/FMPilotServlet), then open the [xDAQ GEM monitoring page](http://srv-s2g18-33-01.cms:20100/urn:xdaq-application:lid=61467). From there, open the "Expert Page" tab and click on "Configure AMCs". After the AMC status passes to "Configured", click on "Start AMC monitoring".

### The fast way

From an `lxplus` SSH session, activate the SSH tunnel to P5, then run the setup script:
```
ssh -Nf cmsusr-tunnel
source /eos/home-a/apellecc/www/gem/ge11/commissioning/vtrx-rssi/setup.sh
```

Start the 5-minute monitoring and update the RSSI trend plot:
```
rssi-update
```

If you stop the command before the end of the 5 minutes, the acquisition is interrupted and the trend plot remains unchanged.

After the acquisition is finished, stop the AMC monitoring from the corresponding button in the xDAQ "Expert Page" tab, then stop the ongoing run from RCMS.

You can find the updated RSSI plot either in `results/summary.png` in the RSSI folder or at [this address](https://apellecc.web.cern.ch/apellecc/gem/ge11/commissioning/vtrx-rssi/results/summary.png).

### Alternative: the customizable way

From an `lxplus` SSH session, move to the RSSI folder and activate the SSH tunnel to P5:
```
ssh -Nf cmsusr-tunnel
cd /eos/home-a/apellecc/www/gem/ge11/commissioning/vtrx-rssi/
```

Start the RSSI monitoring for a 5-minute interval (action to be taken twice a day):
```
./rssi-monitor.py --time 300 --input rssi.txt --output rssi.txt --log log
```

Update the RSSI trend plot: 
```
./rssi-plot.py --input rssi.txt --output results
```

## RSSI monitoring results

![RSSI monitoring](https://apellecc.web.cern.ch/apellecc/gem/ge11/commissioning/vtrx-rssi/results/summary.png "RSSI monitoring")