#!/usr/bin/python3

import os, sys
import argparse

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
plt.style.use('seaborn-whitegrid')

RSSI_PLOT_URL = 'https://apellecc.web.cern.ch/apellecc/gem/ge11/commissioning/vtrx-rssi/results/summary.png'

def main():
    ap = argparse.ArgumentParser(add_help=True)
    ap.add_argument('--input', help='Input file with RSSI current values per chamber', default='rssi.txt')
    ap.add_argument('--output', help='Output folder', default='results')
    ap.add_argument('--long', action='store_true', help='Plot monitoring over a long time (hours)')
    options = ap.parse_args(sys.argv[1:])

    try: os.makedirs(options.output)
    except FileExistsError: pass

    dfInput = pd.read_csv(options.input, sep='\t')

    ndetectors = len(dfInput.detector.unique())
    nrows = 2
    ncols = int(ndetectors/nrows)

    figOverTime, axsOverTime = plt.subplots(nrows, ncols, figsize=(14,12))
    if options.long: figHistogram, axsHistogram = plt.subplots(nrows, ncols, figsize=(14,12))

    # plot data for each detector
    for idet,detector in enumerate(dfInput.detector.unique()):
        dfDetector = dfInput[dfInput.detector==detector]

        row, col = idet//ncols, idet%ncols
        axOverTime = axsOverTime[row][col]
        axOverTime.set_title(detector)
        if options.long:
            axHistogram = axsHistogram[row][col]
            axHistogram.set_title(detector)
            axHistogram.set_xlim(180, 450)
            axHistogram.set_xlabel('RSSI current (µA)')
        
        # plot for all rssis
        for rssi in dfDetector.rssi.unique():
            dfRssi = dfDetector[dfDetector.rssi==rssi]
            print(detector, rssi)
            print(dfRssi, '\n')

            if options.long:
                times, rssiValues = dfRssi.time, dfRssi.value
                axOverTime.plot(times, rssiValues, label=f'RSSI{rssi}')
                axOverTime.set_xlabel('Time (s)')
                axHistogram.hist(rssiValues, bins=5)
            else:
                datesString, rssiValues = dfRssi.date, dfRssi.value
                dates = [ np.datetime64(date.replace(' ', 'T')) for date in datesString ]
                axOverTime.plot(dates, rssiValues, label=f'RSSI{rssi}')
                axOverTime.set_xlabel('Time')
            
            axOverTime.set_ylim(10, 500)
            axOverTime.set_ylabel('RSSI current (µA)')
            axOverTime.legend()
    
    if options.long:
        try: os.makedirs(f'{options.output}/monitoring')
        except FileExistsError: pass
        outputFileName = os.path.basename(options.input)[:-4]
        figOverTime.savefig(f'{options.output}/monitoring/{outputFileName}.png')

        try: os.makedirs(f'{options.output}/histogram')
        except FileExistsError: pass
        figHistogram.savefig(f'{options.output}/histogram/{outputFileName}.png')

    else:
        figOverTime.autofmt_xdate()
        figOverTime.savefig(f'{options.output}/summary.png')
        print(f'RSSI trend updated. You can find the updated plot at {RSSI_PLOT_URL}')
    
if __name__=='__main__': main()
